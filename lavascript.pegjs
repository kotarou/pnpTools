{
  var executionLimit = 10000;

  console.log("===== START =====")
  var dict = {};

  function makeString(item){
    //TODO: check for /* style comments
    if(item[0] == "//")
    {
        // Comment
        var i, j, result = "";
        result = result + item[0];
        j = item[1].length;
        for(i = 0; i < j; i++)
          result = result + item[1][i][1];
        return result;
    }
    else{
      console.log("joining", item);
      var i, j, result = "";
      result = result + item[1];
      if(item.length < 3)
          j = 0;
      else
          j = item[2].length;
      for(i = 0; i < j; i++)
          result = result + item[2][i];
      return result; //.join("");
    }

  }

  function arrayIn(arr, item){
    return arr.indexOf(item) != -1;
  }

  function assign(variable, value){
    dict[variable] = value;
    //console.log("Current dict:");
    //console.log(dict);
  }

  function diceRoll(in_num, sides, explodes){
    console.log("dice rolling", in_num, "d", sides);
        var i = 0, result = 0, num = in_num, rolls = [];
        while(i < num){
            var temp = Math.floor((Math.random() * (sides)) + 1);

            if(temp === sides && explodes)
                  num += 1;
            result += temp;
            rolls.push(temp);
            i++;

            if( i > executionLimit)
                break;
        }
        return {
            action: "decompose",
            type: "sum",
            value: {
                sum: result,
                rolled: rolls.sort()
            }
        };
    }

  function math(operator, left, right){
    //console.log("==MATH==");
    if(operator == "chain"){
        //console.log("chain");
        var i;
        // This is a chained statement
        var l = execute(left);
        //console.log(l);
        //console.log("right ", right);
        for(i=0; i < right.length; i++){
            //console.log(right[i].type, left, right[i].value.right);
            l = math(right[i].type, left, right[i].value.right);
            //console.log(l);
        }
        return l;
    }

    // Order here determines bedmas order - sort of
    if(operator == "^") return Math.pow(execute(left), execute(right));
    if(operator == "/") return execute(left) / execute(right);
    if(operator == "*") return execute(left) * execute(right);
    if(operator == "%") return execute(left) % execute(right);
    if(operator == "+") return execute(left) + execute(right);
    if(operator == "-") return execute(left) - execute(right);


  }

    // Reduce fodder
    function add(a, b) {
        return a + b;
    }

  function execute(input){
    if(typeof(input) === 'number')
        return input;

    //console.log("Execution input:");
    //console.log(input);

    if(input.action === "decompose"){
        if(input.type === "sum")
            return input.value.sum;
        else{
            console.log("dice rolled", input.value.rolled);
            return input.value.rolled;
        }
    }

    if(input.action === "dice"){
        var typ = false;
        if(arrayIn(input.type, "exploding"))
            typ = true;
        var dice = diceRoll(execute(input.value.left), execute(input.value.right), typ);

        if(arrayIn(input.type, "split"))
            dice.type = "split";
        return execute(dice);
    }

    if(input.action === "math"){
        return math(input.type, input.value.left, input.value.right);
    }

    if(input.action === "assign"){
        var value;
        if(input.value.variableValue.action)
            value = execute(input.value.variableValue);
        else
            value = input.value.variableValue;
        dict[input.value.variableName] = value;
        return input.value.variableName + " = " + value;
    }
      /*  value:{
            dice: makeString(nm), //nm.join(""),
            num: val,
            direction: dir
        }*/
    if(input.action === "retrieve"){
        //console.log("retrieve", input);
        if(input.type === "int"){
            return Math.floor(execute(input.value));
        }
        return dict[input.value];
    }

    if(input.action === 'diceOperation'){
        console.log("CHECKING FOR D", input.value.dice, dict[input.value.dice]);
        console.log("dict", dict);
        var d = dict[input.value.dice];

        if(input.type === "split"){
            // TODO: sanitise input
            var num = execute(input.value.num);
            console.log("dice selecting", num, input.value.direction, d.slice(0,input.value.num))
            if(input.value.direction == "low")
                return d.slice(0, num);
            else
                return d.slice(d.length - num, d.length);
        }
        if(input.type === "sum"){
            return d.reduce(add, 0);
        }
    }


    if(input.action === "none"){
        return input.value;
    }

    if(input.action === "resolve"){
        // Resolve a boolean
        var op      = input.value.test.value.op;
        // We need to execute these to get thier values
        var pre     = execute(input.value.test.value.pre);
        var post    = execute(input.value.test.value.post);

        var comp    = false;
        //"<=" / "< " / ">=" / ">" / "==" / "!="

        console.log("IF: " + pre + " " + op + " " + post);
        op = op.replace(" ", "");
        console.log("K" + op + "K");
        if(op == "<=")  comp = (pre <= post);
        if(op == "<")   comp = (pre < post);
        if(op == ">=")  comp = (pre >= post);
        if(op == ">")   comp = (pre > post);
        if(op == "==")  comp = (pre === post);
        if(op == "!=")  comp = !(pre === post);

        console.log("Bool resolved to: " + comp);

        return comp;
    }


    if(input.action === "execute" && input.type === "conditional"){
        console.log("Executing conditional:");
        console.log(input);

        var comp = execute ({
            action: "resolve",
            type: "ifelse",
            value: input.value
        });

        console.log("Conditional result:" + comp);

        var i, results = [];

        if(comp) {
            for(i=0; i < input.value.resTrue.length; i++){
                console.log(input.value.resTrue);
                results.push(execute(input.value.resTrue[i][1]));
            }
        }
        else{
            console.log("testing false");
            console.log(input.value.resFalse);
            if(input.value.resFalse != null)
                for(i=0; i < input.value.resFalse.length; i++){
                    results.push(execute(input.value.resFalse[i][1]));
                }
        }

        return results;

    }

    if(input.action === "execute" && input.type === "while"){
        console.log("Executing while loop:");
        console.log(input);

        var i, j, results = [];
        var comp = true;

        while(comp){
            // Tracking variable
            i = 1 + 1;
            console.log("While loop on iteration: " + i);
            for(j=0; j < input.value.body.length; j++){
                console.log(input.value.body);
                results.push(execute(input.value.body[j][1]));
            }
            //results.push(execute(input.value.body[i][1]));

            comp = execute ({
                action: "resolve",
                type: "boolean",
                value: input.value
            });

            // Stop too many iterations of while occuring
            if(i < executionLimit)
                break;
        }

        return results;

    }

  }

}

START = outs:(FLOW _)* {
    var i, results = [];

    for(i=0; i < outs.length; i++){
        results.push(outs[i][0]);
    }

    return results;
}

// Only point at which executing begins
FLOW = con:CONDITIONAL { return execute(con); } / whl:WHILE { return execute(whl); } /  stm:STATEMENT { return execute(stm); }

STATEMENT   =   com:COMMENT { return {action:"none", type:"comment", value:makeString(com)}; } /
                exp:EXPRESSION _ SEPERATOR { return exp; }

CONDITIONAL = "if" _ comp:BOOLEAN _ "{" _ tru:(_ STATEMENT _ SEPERATOR)* _ "}" _ "else" _ "{" _ fls:(_ STATEMENT _ SEPERATOR)* _ "}" _  {
    //console.log("Constructing conditional:");
    //console.log(comp, tru, fls, text());
    return {
        action: "execute",
        type: "conditional",
        value: {
            test: comp,
            resTrue: tru,
            resFalse: fls
        }
    };
}

WHILE = "while" _ comp:BOOLEAN _ "{" _  tdo:(_ STATEMENT _ SEPERATOR)* _ "}" {
    //console.log("Constructing while loop:");
    //console.log(comp);
    return {
        action: "execute",
        type: "while",
        value: {
            test: comp,
            body: tdo,
        }
    };
}

EXPRESSION  = SUBEXPRESSION / ASSIGNMENT

SUBEXPRESSION = OPERATION / EXECUTION

// TODO: Add chaining of operations
OPERATION = nm:VARIABLENAME ".take(" _ val:EXECUTION _ "," _ dir:("low" / "high") _ ")" {
    return {
        action:"diceOperation",
        type:"split",
        value:{
            dice: makeString(nm), //nm.join(""),
            num: val,
            direction: dir
        }
    }
} / nm:VARIABLENAME ".sum()" {
    return {
        action:"diceOperation",
        type:"sum",
        value:{
            dice: makeString(nm), //nm.join(""),
        }
    }
}

ASSIGNMENT  = "set" _ nm:VARIABLENAME _ "=" _ val:SUBEXPRESSION {
    return {
        action:"assign",
        type:"",
        value:{
            variableName: makeString(nm), //nm.join(""),
            variableValue: val
        }
    }
}

BOOLEAN     = pre1:SUBEXPRESSION _ op1:("<=" / "< " / ">=" / ">" / "==" / "!=") _ post1:SUBEXPRESSION {
    return {
        action: "execute",
        type: "comparison",
        value: {
            pre: pre1,
            op: op1,
            post: post1
        }
    };
}

// Defined as anything that executed to a single output
EXECUTION   = val:(MATH){
    //console.log("EXECUTION");
    //console.log(val);
    return val;
}

MATHCOMPONENT = val:(DICE / SMALLMATHCOMPONENT) {
    return val;
}

SMALLMATHCOMPONENT = "int(" _ nm:MATH _ ")" {
    return {
        action:"retrieve",
        type:"int",
        value: nm
    } ;
} / val:(VARIABLE / INTEGER / BRACKET) {
    return val;
}

DICE = l:SMALLMATHCOMPONENT "d" r:SMALLMATHCOMPONENT exp:"!"? split:".split()"? {
    var typ = [];
    if (exp != null)
        typ.push("exploding");
    else
        typ.push("normal");
    if(split != null)
        typ.push("split");

    return {
        action: "dice",
        type: typ,
        value:
        {
            left: l,
            right: r
        }
    };
}

BRACKET = "(" _ exe:MATH _ ")" {
    return exe;
}

MATH = l:MATHCOMPONENT _ tail:(("+" / "-" / "*" / "/" / "%" / "^") _ MATHCOMPONENT _)* {
    var i, results = [];
    //console.log("math occuring");

    for(i = 0; i < tail.length; i++){
        results.push( {
            action: "math",
            type: tail[i][0],
            value: {
                right: tail[i][2]
                }
            });
    }
    //console.log(results);
    return {
        action: "math",
        type: "chain",
        value:{
            left: l,
            right: results
        }
    };
}




VARIABLE
    =   nm:VARIABLENAME { return {action:"retrieve", type:"", value:makeString(nm)}; } // return dict[makeString(nm)];

INTEGER "integer"
    =   [-]? [0-9]+ { return parseInt(text(), 10); }
//VARIABLENAME
//    =   [a-z] [a-z0-9_-]*
VARIABLENAME
    =   !RESERVED [a-z] [a-z0-9_-]*
RESERVED
    = ("set" / "if" / "else" / "end" / "int(" / "split(")  ![A-Za-z_-]
COMMENT
    =   ("/*" (!"*/" .)* "*/" / "//" (!"\n" .)*)
SEPERATOR
    =   [;,]? [\n\r]?
_ "whitespace"
    =   ([ \t\n\r]*)?
