pnpTools for making playing rpgs a bit less of a paper hassle.

Features under current development:
    graphical diecroller for simple rolls
    language based dice rooler for complex rolls and analysis (determining degrees of success in dark heresy, for example)
    dice roll broadcast to GM for verification purooses
    chat functions for private GM-player ocmmunications in-game

Proposed future features:
    Initiative tracker
    Probability calculator
    Estimated value calculator


Steps to run:
    Bundle the parser into a javascript file that can be imported in browser
        browserify -t brfs main.js > bundle.js

