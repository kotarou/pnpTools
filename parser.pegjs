{
  var dict = {};
}

START = COMMENT _ STATEMENT / STATEMENT
STATEMENT = CONDITIONAL _ (_ SEPERATOR _ COMMENT? _ (CONDITIONAL / SUM))* / SIMPLE

// TODO: Allow for booleans without needing ternary operators
CONDITIONAL = pre:SUM _ comp:("<" / "==" / "!=" / "<=" / ">=" / ">" ) _ post:SUM _ "?"_ tru:START _ ":" _ fls:START _ {
  if(comp === "<" && pre < post)
    return tru;
  if(comp === "==" && pre === post)
    return tru;
  if(comp === "!=" && !(pre === post))
    return tru;
    if(comp === ">" && pre > post)
    return tru;
  if(comp === "<=" && pre <= post)
    return tru;
  if(comp === ">=" && pre < post)
    return tru;
    else
      return fls;
}

SIMPLE = head:SUM _ tail:(_ SEPERATOR _ (CONDITIONAL / SUM))* {
  var results = [head], i;

    for (i = 0; i < tail.length; i++) {
        results.push(tail[i][3]);
      }

      if(results.length === 1)
        return results[0];
      return results;
}

SUM = head:PRODUCT tail:(_ ( '+' / '-' ) _ PRODUCT)* _?{
      var result = head, i;

      for (i = 0; i < tail.length; i++) {
        if (tail[i][1] === "+") { result += tail[i][3]; }
        if (tail[i][1] === "-") { result -= tail[i][3]; }
      }

      return result;
    }

PRODUCT = head:POWER tail:(_ ("/" / "*" / "%" / "\\") _ POWER)* {
      var result = head, i;

      for (i = 0; i < tail.length; i++) {
        if (tail[i][1] === "*") { result *= tail[i][3]; }
        if (tail[i][1] === "/") { result /= tail[i][3]; }
        if (tail[i][1] === "%") { result = result % tail[i][3]; }
        if (tail[i][1] === "\\") { result /= tail[i][3]; }
      }

      return result;
}

POWER = head:TOPLEVEL tail:(_ "^" _ POWER)* {
  // TODO: Accept brackets in exponent
    // ("(" _ POWER _ ")" / POWER)
    var result = head, i;

  for (i = 0; i < tail.length; i++) {
      result = Math.pow(result, tail[i][3]);
    }

    return result;
}

TOPLEVEL =
  val:DICE { return val; } /
  val:DECIMAL { return val; } /
  CASTINT / ASSIGNMENT / VARIABLE /
    op:"-" _ val:TOPLEVEL { return -val; } /
    BRACKET /  STRING

BRACKET = val:DECIMAL { return val; } /
    "(" _ val:DICE _ ")" { return val; } /
    "(" _ val:SUM _ ")" { return val; }

DICE = head:BRACKET? _ "d" _ tail:TOPLEVEL _ exp:"!"?{
  var h = head, result = 0, i = 0, flip = false, explode = true;
    if (head === null)
      h = 1;
    if (exp === null)
      explode = false;

    // TODO: Throw the correct error here
    if(tail < 0)
      return 0;

    if( h < 0){
      h = -h;
        flip = true;
    }

    // TODO: Control Sequences
    while(i < h){
    var temp = Math.floor((Math.random() * (tail)) + 1);
    // Note: this prints during the parsing as well as the execution
    console.log(head+"d"+tail+ " rolled: " + temp);
        if(temp === tail && explode)
          h += 1;
    result += temp;
        i++;
    }

    if(flip)
      return -result;
    else
      return result;

}

// TODO: Implement this
ASSIGNMENT = "set" _ nm:VARIABLENAME _ "=" _ val:SUM {
  dict[nm] = val;
    // TODO: remove commas from printed var
    return "variable " + nm + " set"; //dict[nm]; //dict[name];
}

// TODO: Make this accept non-integers
DECIMAL = [0-9]+  { return parseInt(text(), 10); }

// A variable starts with a letter and is followed by letters and numbers
// TODO: Make sure there is no weirdness around "d" as a variable
VARIABLE = vari:([a-zA-Z_][a-zA-Z0-9_]*){
  return dict[vari];
}
// TODO: Syncing between VARIABLE and VARIABLENAME
VARIABLENAME = ([a-z_][a-z0-9_]*)

STRING = val:("\"" ([a-z0-9 ]*) "\"")

CASTINT = "int(" _ val:SUM _")" {
  return parseInt(val);
}

Integer "integer"
  = [0-9a-z]+ { return parseInt(text(), 10); }

_ "whitespace"
  = [ \t\n\r]*

COMMENT = "//" val:([a-z0-9 ]*) "\n" {
  return {"comment": val.join("")};
}

SEPERATOR
  = ("," / ";")
